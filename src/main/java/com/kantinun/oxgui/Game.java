package com.kantinun.oxgui;

import java.util.Scanner;

/**
 *
 * @author EAK
 */
public class Game {

    private Player o;
    private Player x;
    private int row;
    private int col;
    private Board board;
    Scanner sc = new Scanner(System.in);

    public Game() {
        this.o = new Player('O');
        this.x = new Player('X');
    }

    public void newBoard() {
        this.board = new Board(o, x);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTable() {
        char[][] table = this.board.getTable();
        for (int r = 0; r < table.length; r++) {
            for (int c = 0; c < table[r].length; c++) {
                System.out.print(table[r][c]);
            }
            System.out.println();
        }
    }

    public void showTurn() {
        Player player = board.getCurrentPlayer();
        System.out.println("Turn " + player.getSymbol());
    }

    public void inputRowCol() {
        while (true) {
            System.out.println("Please input row, col: ");
            row = sc.nextInt();
            col = sc.nextInt();
            if (board.setRowCol(row, col)) {
                return;
            }
        }
    }
    
    public boolean isFinish(){
        if(board.isDraw() || board.isWin()){
            return true;
        }
        return false;
    }
    
    public void showResult(){
        if(board.isDraw()){
            showDraw();
        }else if(board.isWin()){
            showWin();
        }
    }
    
    public void showStat(){
        System.out.println(o);
        System.out.println(x);
    }
    
    public void showWin() {
        System.out.println(">>> " + this.board.getCurrentPlayer().getSymbol() + " Win<<<");
    }

    public void showDraw() {
        System.out.println(">>>Draw<<<");
    }
    
}
